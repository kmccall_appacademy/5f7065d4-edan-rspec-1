def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, num = 2)
  string = word
  (num - 1).times do
    string += " " + word
  end
  string
end

def start_of_word(word, num)
  word[0...num]
end

def first_word(string)
  string.split(" ")[0]
end

def titleize(string)
  words = string.split
  words.each_with_index do |word, idx|
    if idx == 0 || important_word?(word) == true
      word == word.capitalize!
    end
  end
  words.join(" ")
end

def important_word?(word)
  unimportant_words = ["and", "the", "over"]
  if unimportant_words.include?(word.downcase)
    return false
  end
  true
end
