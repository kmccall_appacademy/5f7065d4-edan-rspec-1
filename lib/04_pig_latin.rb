def translate(string)
  words = string.split
  words.map {|word| latinize(word)}.join(" ")
end

def latinize(word)
  vowels = "aeiou"
  until vowels.include?(word[0])
    if word[0..1] == "qu"
      word = word[2..-1] + word[0..1]
    else
    word = word[1..-1] + word[0]
  end
  end
  word +"ay"
end
