def ftoc(number)
  ((number - 32) * (5.0 / 9.0)).round
end

def ctof (number)
  ((number * 9) / 5.0).round(2) + 32
end
