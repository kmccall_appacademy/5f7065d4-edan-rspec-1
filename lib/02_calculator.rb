def add (num1, num2)
  num1 + num2
end

def subtract (num1, num2)
  num1 - num2
end

def multiply(array)
  array.reduce(:*)
end

def power(num1, num2)
  num1 ** num2
end

def sum (array)
  unless array.empty?
    return array.reduce(:+)
  end
  0
end

def factorial(num)
  (1..num).reduce(:*) || 1
end
